package security.service;



import java.io.File;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import security.appstarter.SecurityUtils;


//@Service
public class EmailService {

	@Autowired
	public JavaMailSender mailSender;

	@Async
	public void sendConfirmationEmail(String emailAddress, String token) {
		
		MimeMessage message = mailSender.createMimeMessage();;  
		MimeMessageHelper helper;
		try {
			helper = new MimeMessageHelper(message, true);
			helper.setTo(emailAddress);
			helper.addCc("junjar.singh92@gmail.com");
			helper.setFrom("refermebro@gmail.com");
			String href = SecurityUtils.APP_URL + "/confirm?confirmationToken=" + token + "&emailAddress=" + emailAddress;
			helper.setSubject("ReferMeBro: Registration Confirmation");
			helper.setText("<html><body><h3>To confirm your e-mail address, please click the link below: </h3><a href=\""+href+"\">Verify Email</a></body></html>", true);
			System.out.println("<html><body><h3>To confirm your e-mail address, please click the link below: </h3><a href=\""+href+"\">Verify Email</a></body></html>");


		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		mailSender.send(message);

		
		
//		SimpleMailMessage confirmationEmail = new SimpleMailMessage();
//		confirmationEmail.setTo(emailAddress);
//		confirmationEmail.setSubject("ReferMeBro: Registration Confirmation");
//		confirmationEmail.setText("To confirm your e-mail address, please click the link below:\n"
//				+ SecurityUtils.APP_URL + "/confirm?confirmationToken=" + token + "&emailAddress=" + emailAddress);
//		confirmationEmail.setFrom("refermebro@gmail.com");
//
//		mailSender.send(confirmationEmail);
	}
	
	
	@Async
	public boolean sendSimpleMessage(String simpleMessage, String emailAddress) {
		SimpleMailMessage confirmationEmail = new SimpleMailMessage();
		confirmationEmail.setTo(emailAddress);
		confirmationEmail.setSubject("ReferMeBro: Registration Confirmation");
		confirmationEmail.setText(simpleMessage);
		confirmationEmail.setFrom("refermebro@gmail.com");
		mailSender.send(confirmationEmail);
		return true;
	}

	@Async
	public void sendReferalMail(String toEmailAddress,String fromEmailAddress, String jobPosition) {
		MimeMessage message = mailSender.createMimeMessage();

		MimeMessageHelper helper;
		try {
			helper = new MimeMessageHelper(message, true);
			helper.setTo(toEmailAddress);
			helper.addCc("junjar.singh92@gmail.com");
			helper.setSubject("ReferMeBro for open position");
			helper.setText("Hi, PFA resume. Kindly refer me for opened position:" + jobPosition);
			helper.setFrom("refermebro@gmail.com");
			FileSystemResource file = new FileSystemResource(
					new File(SecurityUtils.UPLOADED_FOLDER + fromEmailAddress + ".pdf"));
			helper.addAttachment("resume.pdf", file);

		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		mailSender.send(message);
	}

}
