package security.persistence.dto;

public class GetDetailsResponse extends BaseMessage {
	
	private String company;
	private String designation;
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	

}
