package security.appstarter;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import security.persistence.dao.UserRepository;
import security.persistence.model.ApplicationUser;
import security.service.MyUserDetailsService;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {
	
    private UserRepository userRepository;
	
	private MyUserDetailsService myUserDetailsService;

	public JWTAuthorizationFilter(AuthenticationManager authManager) {
		super(authManager);
	}
	
	public JWTAuthorizationFilter(AuthenticationManager authManager, UserRepository userRepository, MyUserDetailsService detailsService) {
		super(authManager);
		this.userRepository = userRepository;
		this.myUserDetailsService = detailsService;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		String header = req.getHeader(SecurityUtils.HEADER_STRING);

		if (header == null || !header.startsWith(SecurityUtils.TOKEN_PREFIX)) {
			chain.doFilter(req, res);
			return;
		}

		UsernamePasswordAuthenticationToken authentication = getAuthentication(req);

		SecurityContextHolder.getContext().setAuthentication(authentication);
		chain.doFilter(req, res);
	}

	private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
		String token = request.getHeader(SecurityUtils.HEADER_STRING);
		if (token != null) {
			
			final JwtParser jwtParser = Jwts.parser().setSigningKey(SecurityUtils.SECRET.getBytes());

	        final Jws claimsJws = jwtParser.parseClaimsJws(token.replace(SecurityUtils.TOKEN_PREFIX, ""));

	        final Claims claims = (Claims) claimsJws.getBody();

	        final Collection authorities =
	                Arrays.stream(claims.get(SecurityUtils.AUTHORITIES_KEY).toString().split(","))
	                        .map(SimpleGrantedAuthority::new)
	                        .collect(Collectors.toList());

			
			// parse the token.
			String user = Jwts.parser().setSigningKey(SecurityUtils.SECRET.getBytes())
					.parseClaimsJws(token.replace(SecurityUtils.TOKEN_PREFIX, "")).getBody().getSubject();

			ApplicationUser appUser = this.userRepository.findByUsername(user);
			if (appUser != null) {
				//List<GrantedAuthority> grantedAuthorities = (List<GrantedAuthority>) myUserDetailsService.getAuthorities(appUser.getRoles());
				return new UsernamePasswordAuthenticationToken(appUser, appUser.getPassword(), authorities);
			}
			return null;
		}
		return null;
	}

}
