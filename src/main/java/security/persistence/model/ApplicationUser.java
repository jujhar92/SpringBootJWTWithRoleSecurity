package security.persistence.model;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.JoinColumn;

@Entity
@Table(name = "user_account")
public class ApplicationUser {
	
	@Id
	@Column(unique = true, nullable = false)
    private String emailAddress;//@Column(name="EMAILADDRESS")
	
	//@Column(name="USERNAME")
    private String userName;
	
    
	//@Column(name="PASSWORD")
    @Column(length = 60)
    private String password;
	
	//@Column(name="ISEMAILVERIFIED")
    private String isEmailVerified;
	
	//@Column(name="CONFIRMATIONTOKEN")
    private String confirmationToken;
    
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "users_roles", joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "emailAddress"), inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
    private Collection<Role> roles;
    
    public ApplicationUser(String email,String userName,String password,String isEmailVerified,String confirmationToken) {
		this.emailAddress = email;
		this.userName = userName;
		this.password = password;
		this.isEmailVerified = isEmailVerified;
		this.confirmationToken = confirmationToken;
		
	}
    public ApplicationUser() {}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getIsEmailVerified() {
		return isEmailVerified;
	}

	public void setIsEmailVerified(String isEmailVerified) {
		this.isEmailVerified = isEmailVerified;
	}

	public String getConfirmationToken() {
		return confirmationToken;
	}

	public void setConfirmationToken(String confirmationToken) {
		this.confirmationToken = confirmationToken;
	}

	public Collection<Role> getRoles() {
		return roles;
	}

	public void setRoles(Collection<Role> roles) {
		this.roles = roles;
	}
    
	@Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((emailAddress == null) ? 0 : emailAddress.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ApplicationUser user = (ApplicationUser) obj;
        if (!emailAddress.equals(user.emailAddress)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("User [id=").append(emailAddress).append(", roles=").append(roles).append("]");
        return builder.toString();
    }

}
