package security.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import security.persistence.model.ApplicationUser;
import security.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@RequestMapping("/list")
	public List<ApplicationUser> getUserList() {
		return this.userService.getUserList();
	}
	
	@PostMapping("/list1")
	public List<ApplicationUser> getUserLists() {
		return this.userService.getUserList();
	}

}
