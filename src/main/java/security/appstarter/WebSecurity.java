package security.appstarter;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import security.persistence.dao.UserRepository;
import security.service.MyUserDetailsService;

@Configuration
@EnableWebSecurity
public class WebSecurity extends WebSecurityConfigurerAdapter{
	
	private MyUserDetailsService userDetailsService;
	
	@Autowired
	private UserRepository userRepository;

	public WebSecurity(MyUserDetailsService userDetailsService) {
		this.userDetailsService = userDetailsService;
	}
	
	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.cors().and().csrf().disable().authorizeRequests()
				.antMatchers(HttpMethod.POST, SecurityUtils.SIGN_UP_URL).permitAll()
				.antMatchers(HttpMethod.POST, SecurityUtils.LOGIN_URL).permitAll()
				.antMatchers(HttpMethod.POST, SecurityUtils.REST_PASSWORD_URL).permitAll()
				.antMatchers(HttpMethod.GET, SecurityUtils.SEND_OTP_URL).permitAll()
				.antMatchers(HttpMethod.GET, "/user/list").permitAll()
				.antMatchers(HttpMethod.POST, "/user/list1").permitAll()
				//.antMatchers(HttpMethod.GET, "/users/list").permitAll()
				//.antMatchers(HttpMethod.GET, "/users/index.html").permitAll()
				//.antMatchers(HttpMethod.GET, "/","/public/**","/resources/**","/resources/public/**").permitAll()
				//.antMatchers(HttpMethod.GET, "/referme/index.html").permitAll()
				//.antMatchers(HttpMethod.GET, "/users/sendmail").permitAll()
				.antMatchers(HttpMethod.GET, SecurityUtils.CONFIRMATION_URL).permitAll()
				.anyRequest().authenticated()
				.and()
				.addFilter(new JWTAuthenticationFilter(authenticationManager()))
				.addFilter(new JWTAuthorizationFilter(authenticationManager(),userRepository,userDetailsService));
	}
	
	@Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder());
	}

	@Bean
	CorsConfigurationSource corsConfigurationSource() {
		CorsConfiguration configuration = new CorsConfiguration();
		//configuration.setAllowedOrigins(Arrays.asList("*","https://localhost:4200/","https://localhost:4200/**","http://localhost:4200/","localhost:4200/"));
		//configuration.setAllowedMethods(Arrays.asList("GET","POST"));
		//configuration.addAllowedOrigin("http://localhost:4200");
		configuration.addAllowedOrigin("*");
		configuration.addAllowedHeader("*");
		configuration.addAllowedMethod("*");
		configuration.setAllowCredentials(true);
		final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		//source.registerCorsConfiguration("/**", new CorsConfiguration().applyPermitDefaultValues());
		source.registerCorsConfiguration("/**",configuration );
		return source;
	}


}
