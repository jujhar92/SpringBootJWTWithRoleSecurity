package security.persistence.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import security.persistence.model.ApplicationUser;

public interface UserRepository extends JpaRepository<ApplicationUser, Long> {
	
	//ApplicationUser findByEmail(String emailAddress);
	
	@Query("SELECT u FROM ApplicationUser u WHERE LOWER(u.emailAddress) = LOWER(:emailAddress)")
	ApplicationUser findByEmail( @Param("emailAddress") String emailAddress);

	@Override
	void delete(ApplicationUser user);
	
	@Query("SELECT u FROM ApplicationUser u WHERE LOWER(u.userName) = LOWER(:userName)")
	ApplicationUser findByUsername( @Param("userName") String userName);
	
	@Transactional
	@Modifying
	@Query("Update ApplicationUser set password = :password WHERE LOWER(emailAddress) = LOWER(:emailAddress)")
	void updateUserPassword( @Param("password") String password,@Param("emailAddress") String emailAddress);
	
	
	@Query("SELECT u FROM ApplicationUser u WHERE LOWER(u.emailAddress) = LOWER(:emailAddress) and u.confirmationToken = :confirmationToken")
	ApplicationUser findUserByEmailAndToken( @Param("emailAddress") String emailAddress,@Param("confirmationToken") String confirmationToken);


}
