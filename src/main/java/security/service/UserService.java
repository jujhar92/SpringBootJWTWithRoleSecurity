package security.service;

import static java.util.Collections.emptyList;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import security.appstarter.SecurityUtils;
import security.persistence.dao.UserRepository;
import security.persistence.dto.BaseMessage;
import security.persistence.dto.RegistrationForm;
import security.persistence.dto.RestPasswordRequest;
import security.persistence.dto.UpdatePasswordRequest;
import security.persistence.model.ApplicationUser;


@Service
public class UserService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	//@Autowired
	//private EmailService emailService;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public List<ApplicationUser> getUserList() {
		List<ApplicationUser> users = new ArrayList<ApplicationUser>();
		this.userRepository.findAll().forEach(users::add);
		return users;
	}

	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		ApplicationUser applicationUser = userRepository.findByUsername(userName);
		if (applicationUser == null) {
			throw new UsernameNotFoundException(userName);
		}
		return new User(applicationUser.getUserName(), applicationUser.getPassword(), emptyList());
	}

	public ApplicationUser getUserDetails(String email) {
		return this.userRepository.findByEmail(email);
	}

	
	public BaseMessage updateUserPassword(UpdatePasswordRequest appUser) 
	{
		BaseMessage baseMessage = new BaseMessage();
		baseMessage.setStatus(false);
		if (appUser.getNewPassword() == null) 
		{
			baseMessage.setMessage("please provide new password");
		} 
		else if (appUser.getConfirmNewPassword() == null) 
		{
			baseMessage.setMessage("confirm password is empty");
		} 
		else if (!appUser.getConfirmNewPassword().equals(appUser.getNewPassword())) 
		{
			baseMessage.setMessage("confirm password doesn't match with provided new password");
		} 
		else if (appUser.getCurrentPassword() == null) 
		{
			baseMessage.setMessage("current password is not provided");
		} 
		else 
		{
			ApplicationUser foundUser = this.userRepository.findByEmail(appUser.getEmailAddress());
			boolean passwordMatches = bCryptPasswordEncoder.matches(appUser.getCurrentPassword(),
					foundUser.getPassword());
			
			if (!passwordMatches) 
			{
				baseMessage.setMessage("Current password is not correct");
			} 
			else 
			{
				this.userRepository.updateUserPassword(bCryptPasswordEncoder.encode(appUser.getNewPassword()),
						appUser.getEmailAddress());
				
				baseMessage.setStatus(true);
				baseMessage.setMessage("Password updated successfully");
			}
		}
		return baseMessage;
	}

	public BaseMessage signUp(RegistrationForm appUser) 
	{
		BaseMessage baseMessage = new BaseMessage();
		baseMessage.setStatus(false);
		if (appUser.getUserName() == null) 
		{
			baseMessage.setMessage("Please provide your name");
		} 
		else if (appUser.getEmailAddress() == null) 
		{
			baseMessage.setMessage("Please provide email address");
		} 
		else if (appUser.getCompany() == null) 
		{
			baseMessage.setMessage("Please provide name of your organisation");
		} 
		else if (appUser.getDesignation() == null) 
		{
			baseMessage.setMessage("Please provide your designation in provide organisation");
		} 
		else if (appUser.getPassword() == null) 
		{
			baseMessage.setMessage("Please enter password, it is mandatory to create account for you");
		} 
		else 
		{
			ApplicationUser foundUser = this.userRepository.findByEmail(appUser.getEmailAddress());
			
			if (foundUser != null && "Y".equalsIgnoreCase(foundUser.getIsEmailVerified())) 
			{
				baseMessage.setMessage("You have already a accound with provided email: " + appUser.getEmailAddress());
			} 
			else 
			{
				String encryptedPassword = bCryptPasswordEncoder.encode(appUser.getPassword());
				
				String randomToken = UUID.randomUUID().toString();
				
				ApplicationUser registeringUser = new ApplicationUser(appUser.getEmailAddress(), appUser.getUserName(),
						encryptedPassword, "N", randomToken);
				
				this.userRepository.save(registeringUser);
				
				//this.emailService.sendConfirmationEmail(appUser.getEmailAddress(), randomToken);
				
				if (appUser.getResume() != null) 
				{
					BaseMessage baseMessage2 = uploadResume(appUser.getResume(), appUser.getEmailAddress());
					
					if (!baseMessage2.getStatus()) 
					{
						baseMessage.setMessage(
								"Account created successfully! For confirmation click on url link sent to your registered email! "
										+ baseMessage2.getMessage());
					} 
					else 
					{
						baseMessage.setMessage(
								"Account created successfully! For confirmation click on url link sent to your registered email! Resume uploaded successfully!");
					}
				} 
				else 
				{
					baseMessage.setMessage(
							"Account created successfully! For confirmation click on url link sent to your registered email! please upload resume by editing profile");
				}

				baseMessage.setStatus(true);
			}
		}
		return baseMessage;
	}

	private BaseMessage uploadResume(MultipartFile resume, String emailAddress) 
	{
		BaseMessage baseMessage = new BaseMessage();
		baseMessage.setStatus(true);
		baseMessage.setMessage("Successfully uploaded resume!");
		
		File directory = new File(SecurityUtils.UPLOADED_FOLDER);

		if (!directory.exists()) 
		{
			if (!directory.mkdirs()) 
			{
				baseMessage.setMessage("Not able to upload resume. Please try after some time");
				baseMessage.setStatus(false);
				return baseMessage;
			}
		}
		FileOutputStream fos = null;
		
		File resumeFile = new File(SecurityUtils.UPLOADED_FOLDER + File.separator + emailAddress + ".pdf");
		try 
		{
			fos = new FileOutputStream(resumeFile,false); 
			fos.write(resume.getBytes());

			//resume.transferTo(resumeFile);
		} 
		catch (IllegalStateException e) 
		{
			baseMessage.setMessage("Not able to upload resume. Please try after some time");
			baseMessage.setStatus(false);
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			baseMessage.setMessage("Not able to upload resume. Please try after some time");
			baseMessage.setStatus(false);
			e.printStackTrace();
		}
		finally {
			if(fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}
		return baseMessage;

	}

	

	
	private String generateRestPassword() {
		int len = 8;
		String Capital_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String Small_chars = "abcdefghijklmnopqrstuvwxyz";
        String numbers = "0123456789";
                String symbols = "!@#$%^&*_=+-/.?<>)";
 
 
        String values = Capital_chars + Small_chars +
                        numbers + symbols;
 
        // Using random method
        Random rndm_method = new Random();
 
        char[] password = new char[len];
 
        for (int i = 0; i < len; i++)
        {
            // Use of charAt() method : to get character value
            // Use of nextInt() as it is scanning the value as int
            password[i] =
              values.charAt(rndm_method.nextInt(values.length()));
        }
        return password.toString();
	}
	public BaseMessage resetPassword(RestPasswordRequest restPasswordRequest) {
		BaseMessage baseMessage = new BaseMessage();
		baseMessage.setMessage("Password reset !");
		baseMessage.setStatus(false);
		if(restPasswordRequest.getNewPassword() == null) {
			baseMessage.setMessage("Please provide new password");
		}
		else if(restPasswordRequest.getEmailAddress() == null) {
			baseMessage.setMessage("Please provide email address");
		}
		else if(restPasswordRequest.getOtp() == null) {
			baseMessage.setMessage("Please provide OTP sent to your registered Email");
		}
		ApplicationUser appUser = this.userRepository.findByEmail(restPasswordRequest.getEmailAddress());
		if(appUser == null) {
			baseMessage.setMessage("Provided email address is not registered !");
		}
		else {
			if(restPasswordRequest.getOtp().equals(appUser.getConfirmationToken())) {
				String encryptedPassword = bCryptPasswordEncoder.encode(restPasswordRequest.getNewPassword());
				appUser.setPassword(encryptedPassword);
				this.userRepository.save(appUser);
				baseMessage.setStatus(true);
			}
			else {
				baseMessage.setMessage("Invalid OTP");
			}
			
		}
		
		
		return baseMessage;
	}
	public BaseMessage sendOTP(String emailAddress) {
		BaseMessage baseMessage = new BaseMessage();
		baseMessage.setMessage("Sent OTP to your registered email");
		baseMessage.setStatus(true);
		ApplicationUser appUser = this.userRepository.findByEmail(emailAddress);
		if(appUser == null) {
			baseMessage.setMessage("Provided email address is not registered !");
			baseMessage.setStatus(false);
		}
		else {
			String otp = generateOTP();
			appUser.setConfirmationToken(otp);
			this.userRepository.save(appUser);
			//this.emailService.sendSimpleMessage("Your OTP:"+otp, appUser.getEmailAddress());
		}		
		return baseMessage;
	}
	public String generateOTP() {
		int len = 4;
		// Using numeric values
        String numbers = "0123456789";
 
        // Using random method
        Random rndm_method = new Random();
 
        char[] otp = new char[len];
 
        for (int i = 0; i < len; i++)
        {
            // Use of charAt() method : to get character value
            // Use of nextInt() as it is scanning the value as int
            otp[i] =
             numbers.charAt(rndm_method.nextInt(numbers.length()));
        }
        return otp.toString();
	}
	
	public String activateUserAccount(String emailAddress, String confirmationToken) {
		ApplicationUser user = this.userRepository.findUserByEmailAndToken(emailAddress, confirmationToken);

		if (user == null) 
		{
			return "Invalid token or expired! Please register again";
		} 
		else 
		{
			user.setIsEmailVerified("Y");
			this.userRepository.save(user);
			return "Your account activated!";
		}
	}

	public void sendtestm() 
	{
		//EmailSender.send("refermebro@gmail.com", "maneetsingh", "junjar.singh92@gmail.com", "test", "testmail");
	}
	
	

}
