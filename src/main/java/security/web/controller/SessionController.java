package security.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import security.appstarter.SecurityUtils;
import security.persistence.dao.UserRepository;
import security.persistence.dto.AppUserDto;
import security.persistence.dto.BaseMessage;
import security.persistence.dto.LoginResponse;
import security.persistence.dto.RegistrationForm;
import security.persistence.model.ApplicationUser;
import security.service.UserService;

@RestController
@RequestMapping("/users")
public class SessionController {
	
	@Autowired
	private UserService userService;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
    private AuthenticationManager authenticationManager;

	
	@PostMapping("/sign-up")
	public BaseMessage signUp(@ModelAttribute RegistrationForm registrationForm) 
	{
		return this.userService.signUp(registrationForm);
		// res.addHeader(SecurityUtils.HEADER_STRING,
		// SecurityUtils.TOKEN_PREFIX + " " +
		// SecurityUtils.generateToken(user.getUserName()));
	}

	@PostMapping("/login")
	public LoginResponse login(@RequestBody AppUserDto user) 
	{
		LoginResponse loginResponse = new LoginResponse();
		if (user == null) 
		{
			loginResponse.setIsAuthenticated(false);
			loginResponse.setStatus(false);
			loginResponse.setMessage("User details is not provided");
		} 
		else if (user.getEmailAddress() == null) 
		{
			loginResponse.setIsAuthenticated(false);
			loginResponse.setStatus(false);
			loginResponse.setMessage("email-address is not provided");
		} 
		else if (user.getPassword() == null) 
		{
			loginResponse.setIsAuthenticated(false);
			loginResponse.setStatus(false);
			loginResponse.setMessage("password is not provided");
		}

		ApplicationUser foundUser = userRepository.findByEmail(user.getEmailAddress());

		if (foundUser == null) 
		{
			loginResponse.setIsAuthenticated(false);
			loginResponse.setStatus(false);
			loginResponse.setMessage("Email:" + user.getEmailAddress() + " is not registered, please sign-up");
		} 
		else 
		{
			if("N".equalsIgnoreCase(foundUser.getIsEmailVerified())) 
			{
				loginResponse.setIsAuthenticated(false);
				loginResponse.setStatus(false);
				loginResponse.setMessage("Email:" + user.getEmailAddress() + " is registered, But you haven't verified it. Please click on confirmation click sent to your mail");
			}
			else 
			{
				boolean passwordMatches = bCryptPasswordEncoder.matches(user.getPassword(), foundUser.getPassword());
				
				if (passwordMatches) 
				{
					loginResponse.setIsAuthenticated(true);
					loginResponse.setStatus(true);
					loginResponse.setMessage("User authenticated successfully");
					// res.addHeader(SecurityUtils.HEADER_STRING,
					// SecurityUtils.TOKEN_PREFIX + " " +
					// SecurityUtils.generateToken(foundUser.getUserName()));
					
					final Authentication authentication = authenticationManager.authenticate(
			                new UsernamePasswordAuthenticationToken(
			                        user.getUserName(),
			                        user.getPassword()
			                )
			        );
			        SecurityContextHolder.getContext().setAuthentication(authentication);
			        final String token = SecurityUtils.TOKEN_PREFIX + " " +SecurityUtils.generateToken(authentication);
					
					//String token = SecurityUtils.TOKEN_PREFIX + " " + SecurityUtils.generateToken(foundUser.getUserName());
					loginResponse.setJwtToken(token);
					loginResponse.setUserName(foundUser.getUserName());

				} 
				else 
				{
					loginResponse.setIsAuthenticated(false);
					loginResponse.setStatus(false);
					loginResponse.setMessage("Incorrect password provided");

				}				
			}
			
		}
		return loginResponse;
	}
	
	@RequestMapping("/confirm")
	public String activateUserAccount(@RequestParam("emailAddress") String emailAddress,
			@RequestParam("confirmationToken") String confirmationToken) 
	{
		return this.userService.activateUserAccount(emailAddress, confirmationToken);

	}
	
	


}
